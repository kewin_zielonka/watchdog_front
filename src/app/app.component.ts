﻿import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { AlertService, AuthenticationService, PermissionsEngine } from './_services/index';
import { Router, ActivatedRoute } from '@angular/router';
import { RolesEnum } from './_models/index';
import { environment } from '../environments/environment';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Rx';
import {Message} from 'primeng/primeng';
import {TranslateService} from '@ngx-translate/core';
import {SplitButtonModule} from 'primeng/primeng';
import {MenuItem} from 'primeng/primeng';
import {ToolbarModule} from 'primeng/primeng';

@Component({
    moduleId: module.id, 
    selector: 'app',
    styleUrls: ['app.component.css' ],
    templateUrl: 'app.component.html'
})

export class AppComponent implements OnInit {
    rolesEnum = RolesEnum;
    msgs: Message[] = [];
    private checkedTime = 0;
    private url = environment.url;  // URL to web api
    languages: MenuItem[];

    constructor(
        private _permissionsEngine: PermissionsEngine,
        private http: Http,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private translate: TranslateService) {
            translate.addLangs(['en', 'pl', 'de']);
            translate.setDefaultLang('en');
            translate.use('pl');
            this.initializeLanguageItems();
        }
    
    initializeLanguageItems(){
        this.languages = [
            {label: "Polski", command: () => {
                this.changeLang('pl');
            }},
            {label: "Angielski", command: () => {
                this.changeLang('en');
            }},
            {label: "Niemiecki", command: () => {
                this.changeLang('de');
            }}
        ];
    }
    
    changeLang(lang: string) {
        this.translate.use(lang);
    }

    public ngOnInit() {
    }
    ngAfterViewChecked() {
        this.isLoggedinWorker();
    }
    logout() {
        this.authenticationService.logout()
            .subscribe(
            data => {
            },
            error => {
                this.router.navigate(['/login']);
            });
    }
    private isLoggedinWorker() {

        if (Date.now() - this.checkedTime > 5000 && (localStorage.getItem('currentUser') != null)) {
            this.checkedTime = Date.now();
            this.checkLoggedIn();
        }
    }

    private checkLoggedIn() {
      
        this.http.get(this.url + '/rest/ping', { withCredentials: true}).map((res: Response) => {
        }).catch((error: any) => {
            this.logout();
            return Observable.of(false);
        }).subscribe();

        
        
    }

}