﻿import { Component, OnInit,Renderer,ElementRef  } from '@angular/core';

import { Project,User,Department,Customer,Task,Validation } from '../_models/index';

import { ProjectService,UserService,DepartmentsService,CustomersService,RaportService } from '../_services/index';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import {DataTableModule,SharedModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';
import {InputTextModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
import {PickListModule} from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';
import { ConvertToSelectedItems } from '../helpers/index';
import { RolesEnum } from '../_models/index';
import {TooltipModule} from 'primeng/primeng';
import {CheckboxModule} from 'primeng/primeng';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {MenuItem} from 'primeng/primeng';
import { DownloadType } from '../_models/index';
import {ConfirmDialogModule,ConfirmationService} from 'primeng/primeng';

@Component({
    moduleId: module.id,
    templateUrl: 'project.component.html',
})
export class ProjectComponent implements OnInit {
    model: any = {};  
inputDisable: any = {};    
user:User;
project:Project
projects: Project[];
taskList:Task[] = [];
projectsItems:SelectItem[];
projectsFilterItems:SelectItem[];
msgs: Message[] = [];
validation: Validation[] = [];
editRaport:boolean;
downloadTypeItems: MenuItem[];
downloadType: DownloadType;

constructor(private confirmationService: ConfirmationService, private projectService: ProjectService ,private userService:UserService,private customersService:CustomersService,private departmentsService:DepartmentsService,private raportService: RaportService ,private converter:ConvertToSelectedItems,el: ElementRef, renderer: Renderer)
{
    this.loadAllProjects(); 
    this. inputDisable.customer = true;
    this.model.toDate = new Date();
    this.model.fromDate = new Date();
    this.model.fromDate.setUTCDate(1);
    this.model.date = new Date();
}
ngOnInit(){
    this.model.startTime  = new Date(3600000*8-3600000);
    this.model.endTime  = new Date(3600000*16-3600000);
    this.model.sumTime  = new Date(3600000*8-3600000);
    this.initializeDownloadItems();
}

initializeDownloadItems(){
    this.downloadTypeItems = [
        {label: DownloadType[DownloadType.PDF], command: () => {
            this.downloadType = DownloadType.PDF; 

            if(this.taskList.length != 0)
                this.raportService.downloadRaport(this.generateDownloadLink(), this.downloadType);
            else
                this.msgs.push({severity:'error', summary:'Błąd', detail: 'Lista jest pusta '});           
        }},
        {label: DownloadType[DownloadType.XLS], command: () => {
            this.downloadType = DownloadType.XLS;

            if(this.taskList.length != 0)
                this.raportService.downloadRaport(this.generateDownloadLink(), this.downloadType);
            else
                this.msgs.push({severity:'error', summary:'Błąd', detail: 'Lista jest pusta '}); 
        }}
    ];
}

showDeleteProjectConfirm(id:string){
        this.confirmationService.confirm({
            message: 'Czy napewno chcesz usunąć czas pracy?',
            header: 'Wymagane potwierdzenie',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.deleteTask(id);
            },
            reject: () => {
                this.msgs = [{severity:'warn', summary:'Brak potwierdzenia', detail:'Nie zezwolono na usunięcie'}];
            }
        });
}

showSuccess() {
    this.msgs = [];
    this.msgs.push({severity:'success', summary:'Udało się', detail:'Raport został dodany!'});
}
calcHours(){
    if( this.model.endTime!=null&& this.model.startTime!=null)
    {
        this.model.startTime.setSeconds(0);
        this.model.endTime.setSeconds(3);
        this.model.sumTime  = new Date((this.model.endTime.getTime() - this.model.startTime.getTime())-3600000); 
    }
    if( this.model.taskEndTime!=null&& this.model.taskStartTime!=null)
    {
        this.model.taskStartTime.setSeconds(0);
        this.model.taskEndTime.setSeconds(3);
        this.model.taskTime  = new Date((this.model.taskEndTime.getTime() - this.model.taskStartTime.getTime())-3600000); 
    }
}
removeHours(){
    this.model.startTime = null;
    this.model.endTime = null;
    this.model.taskStartTime = null;
    this.model.taskEndTime = null;
}
private loadAllProjects() {
    this.projectService.getAll().subscribe((res:Response) => {
        this.projects = JSON.parse(res.text());
        this.projectsItems =  this.converter.convertProjects(this.projects,false);
        this.projectsFilterItems =  this.converter.convertProjects(this.projects,true);
    }
    );
}
addTask() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    var startTime;
    var endTime;
    if(this.model.startTime==null||this.model.endTime==null)
    {
        endTime=null;
        startTime=null;
    }
    else
    {
        endTime=(this.model.endTime.getHours()*3600 + this.model.endTime.getMinutes()*60 )*1000;
        startTime=(this.model.startTime.getHours()*3600 + this.model.startTime.getMinutes()*60 )*1000;
    }
    this.msgs = [];
    this.addValidation();
    this.validation.forEach(function (valid) {
        if(valid.value==null||valid.value=="")
        {
            this.msgs.push({severity:'error', summary:'Błąd', detail:valid.detail});
        }
    }, this);
    if(this.msgs.length==0)
    {
        let taskToSave = {userId: this.user.id, projectId: this.model.project,description:this.model.description,startTime:startTime, endTime:endTime, hours:(this.model.sumTime.getHours()*3600 + this.model.sumTime.getMinutes()*60 )*1000,  date: this.model.date.toLocaleDateString()};
        this.projectService.saveTask(taskToSave).subscribe(() => {
            this.refreshTable();
            this.showSuccess();
        });
    }
}
addValidation()
{
    this.validation=[];
    this.validation.push({value:this.model.project,detail:'Wypełnij pole projektu'});
    this.validation.push({value:this.model.description,detail:'Wypełnij pole opsiu'});
    this.validation.push({value:this.model.date,detail:'Wypełnij pole daty'});
    this.validation.push({value:this.model.sumTime,detail:'Wypełnij pole czasu'});
  
}
deleteTask(id:string) {
    this.projectService.deleteTask(id).subscribe(() => {
        this.refreshTable();
    });
}
createTaskUI(id:string) {
    this.projectService.getById(id).subscribe((res:Response) => {
        var task = JSON.parse(res.text());
        this.model.id=  task.id;
        this.model.taskProjectId= task.projectId;
        this.model.taskDate=  new Date(task.date);
        this.model.taskStartTime = new Date("1992-06-26 "+ task.fromH);
        this.model.taskEndTime = new Date("1992-06-26 "+ task.toH);
        this.model.taskTime = new Date("1992-06-26 "+ task.hours);
        this.model.taskDescription= task.description
        this.showDialog();
    }
    );
}
close()
{
    this.model.id=  '';
    this.model.taskProjectId=  '';
    this.model.taskDate=  '';
    this.model.taskStartTime = '';
    this.model.taskEndTime =  '';
    this.model.taskTime =  '';
    this.model.taskDescription=  '';
    this.editRaport = false;
}
generateDownloadLink()
{
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    var link = 'userprojId&value='+this.user.id+'&value2=' + this.model.selectedProject+'&to='+this.model.toDate.getDate()+'e'+(this.model.toDate.getMonth()+1)+'e'+this.model.toDate.getFullYear()+'&from='+this.model.fromDate.getDate()+'e'+(this.model.fromDate.getMonth()+1)+'e'+(1900+this.model.fromDate.getYear());
    // this.raportService.downloadRaport(link);
    return link;
}
saveTask() {
    this.editRaport = false;
    this.user = JSON.parse(localStorage.getItem('currentUser'));    
    let taskToSave = {userId: this.user.id, projectId: this.model.project,description:this.model.description,startTime:(this.model.startTime.getHours()*3600 + this.model.startTime.getMinutes()*60 )*1000, endTime:(this.model.endTime.getHours()*3600 + this.model.endTime.getMinutes()*60 )*1000,   date: this.model.date.toLocaleDateString()};
    this.projectService.saveTask(taskToSave).subscribe(() => {
        this.refreshTable();
    });
}
editTask() {
    this.editRaport = false;
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    var startTime;
    var endTime;
    if(this.model.taskEndTime==null||this.model.taskStartTime==null)
    {
        endTime=null;
        startTime=null;
    }
    else
    {
        endTime=(this.model.taskEndTime.getHours()*3600 + this.model.taskEndTime.getMinutes()*60 )*1000;
        startTime=(this.model.taskStartTime.getHours()*3600 + this.model.taskStartTime.getMinutes()*60 )*1000;
    }
    
    let taskToSave = {userId: this.user.id,id:this.model.id,projectId: this.model.taskProjectId,description:this.model.taskDescription,startTime:startTime, endTime:endTime, hours:(this.model.taskTime.getHours()*3600 + this.model.taskTime.getMinutes()*60 )*1000,   date: this.model.taskDate.toLocaleDateString()};
    this.projectService.saveTask(taskToSave).subscribe(() => {
        this.refreshTable();
        this.showSuccess();
    });
}
showProjects()
{
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    let workHoursSerch = {userId: this.user.id, projectId: this.model.selectedProject,dateFrom: this.model.fromDate.toLocaleDateString(),dateTo: this.model.toDate.toLocaleDateString()};
    this.projectService.getUserWorkHours(workHoursSerch).subscribe((res:Response) => {       
        this.taskList = JSON.parse(res.text());
    });
    this.raportService.getDetails(workHoursSerch) .subscribe((res:Response) => {
        
        var details = JSON.parse(res.text());
        this.model.summary = details.summary;
    });
}
refreshTable()
{
    var projectId;    
    projectId = this.model.project
    this.model.selectedProject=projectId;
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    let workHoursSerch = {userId: this.user.id, projectId:projectId,dateFrom: this.model.fromDate.toLocaleDateString(),dateTo: this.model.toDate.toLocaleDateString()};
    this.projectService.getUserWorkHours(workHoursSerch).subscribe((res:Response) => {       
        this.taskList = JSON.parse(res.text());
    });
    this.raportService.getDetails(workHoursSerch) .subscribe((res:Response) => {
        
        var details = JSON.parse(res.text());
        this.model.summary = details.summary;
    });
}
rowStyles(rowData: any, rowIndex: number): string {
    let style:string;
if(rowData.active=='0')
{
    style='gray';
}
return style;
}
showDialog() {
    this.editRaport = true;
}
}