﻿import { Component, OnInit,Renderer,ElementRef  } from '@angular/core';
 
import { Role,Validation} from '../_models/index';
import { RoleService } from '../_services/index';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import {DataTableModule,SharedModule} from 'primeng/primeng';
import {InputTextModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
import {InputTextareaModule} from 'primeng/primeng';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';

@Component({
    moduleId: module.id,
    templateUrl: 'roles.component.html',
})
 
export class RolesComponent implements OnInit {
    model: any = {};    
    inputDisable: any = {};    

    roles:Role[];
    role:Role;
    display: boolean = false;
    create:boolean=true;
    msgs: Message[] = [];
    validation: Validation[] = [];

 
    constructor(private roleService: RoleService,el: ElementRef, renderer: Renderer) {
       this.loadAllRoles();

    }
    ngOnInit(){
      
    }
    clearRoleUI()
    {
        this.display = false;
        this.model  = {};
        this. create = true;
        this. inputDisable= {};

    }
    createRoleUI(role:Role)
    {
       this. create = false;
      this.model.role= role.role;
      this.model.description= role.description;
      this.inputDisable.name=true;
      this.showDialog();
    }
    addValidation()
    {
        this.validation=[];
        this.validation.push({value:this.model.role,detail:'Wypełnij pole nazwy'});

    }
    displayButtonAction()
    {
        this.msgs = [];
        this.addValidation();
        this.validation.forEach(function (valid) {
            if(valid.value==null||valid.value=="")
            {
                this.msgs.push({severity:'error', summary:'Błąd', detail:valid.detail});
            }
        }, this);
        if(this.msgs.length==0)
        {
            if(this.create)
            {
                this.roleService.create(this.model).subscribe((res:Response) => {
                    this. loadAllRoles();
                }            );}
            else
            {
                this.roleService.update(this.model).subscribe((res:Response) => {
                    this. loadAllRoles();
                });
            }
            this.clearRoleUI();   
            
        }
    }
   createRole()
   {   
      if(this.model.password==this.model.subpassword)
          {
      this.roleService.create(this.model).subscribe((res:Response) => {
         this. loadAllRoles();
      }
      
);
     this.clearRoleUI();          }
   
   }
    deleteRole(role:string) {
      this.roleService.delete(role).subscribe(() => { this.loadAllRoles() });
    }
    showDialog() {
        this.display = true;
    }
    editRole(role:string)
    {
        this.roleService.getById(role).subscribe((res:Response) => {
            this.role = JSON.parse(res.text());
            this.createRoleUI(this.role);
        }
        );
    }
    private loadAllRoles() {
        
        this.roleService.getAll().subscribe((res:Response) => {
              this.roles = JSON.parse(res.text());
          }
          
  );
     
    }
}