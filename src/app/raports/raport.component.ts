﻿import { Component, OnInit, Renderer, ElementRef } from '@angular/core';

import { Project, User, Department, Customer, Raport, Validation } from '../_models/index';

import { RaportService, ProjectManagmentService, UserService, DepartmentsService, CustomersService, PermissionsEngine } from '../_services/index';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { InputTextModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { PickListModule } from 'primeng/primeng';
import { SelectItem } from 'primeng/primeng';
import { ConvertToSelectedItems } from '../helpers/index';
import { RolesEnum } from '../_models/index';
import { DownloadType } from '../_models/index';
import { TooltipModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { Message } from 'primeng/primeng';
import {MenuItem} from 'primeng/primeng';

@Component({
    moduleId: module.id,
    templateUrl: 'raport.component.html',
})

export class RaportComponent implements OnInit {
    model: any = {};
    user: User;
    rapotrTypes: SelectItem[];
    selectedTypeItems: SelectItem[];
    customersList: Customer[];
    users: User[];
    departmentsList: Department[];
    projectsList: Department[];
    raportsList: Raport[] = [];
    serchData: {};
    msgs: Message[] = [];
    validation: Validation[] = [];
    rolesEnum = RolesEnum;
    downloadTypeItems: MenuItem[];
    downloadType: DownloadType;
    editRaport: boolean;

    constructor(private _permissionsEngine: PermissionsEngine, private raportService: RaportService, private projectService: ProjectManagmentService, private userService: UserService, private customersService: CustomersService, private departmentsService: DepartmentsService, private converter: ConvertToSelectedItems, el: ElementRef, renderer: Renderer) {
        this.loadAllProjects();
        this.model.toDate = new Date();
        this.model.fromDate = new Date();
        this.model.fromDate.setUTCDate(1);     
    }

    ngOnInit() {
        this.initializeDownloadItems();
    }


    initializeDownloadItems(){
        this.downloadTypeItems = [
            {label: DownloadType[DownloadType.PDF], command: () => {
                this.downloadType = DownloadType.PDF;

                if(this.raportsList.length != 0)
                    this.downloadRaport();
                else
                    this.msgs.push({severity:'error', summary:'Błąd', detail: 'Lista jest pusta'});   
            }},
            {label: DownloadType[DownloadType.XLS], command: () => {
                this.downloadType = DownloadType.XLS;

                if(this.raportsList.length != 0)
                    this.downloadRaport();
                else
                    this.msgs.push({severity:'error', summary:'Błąd', detail: 'Lista jest pusta '});  
            }}
        ];
    }



    changeTaskStatus(id: string) {
        this.raportService.changeTaskStatus(id).subscribe(() => {
            this.filterRaports();
        });
    }

    private loadAllProjects() {
        this.rapotrTypes = [];
        this.rapotrTypes.push({ label: 'Wybierz', value: 'null' });
        if (this._permissionsEngine.hasPermission([this.rolesEnum.SUPERVISOR.toString()])) {
            this.rapotrTypes.push({ label: 'Użytkownik', value: 'user' });
        }
        this.rapotrTypes.push({ label: 'Projekt', value: 'project' });
        this.rapotrTypes.push({ label: 'Dział', value: 'department' });
        this.rapotrTypes.push({ label: 'Klient', value: 'customer' });
    }
    fillCombo() {
        switch (this.model.selectedType) {
            case 'project':
                this.projectService.getAll().subscribe((res: Response) => {
                    this.projectsList = JSON.parse(res.text());
                    this.selectedTypeItems = this.converter.convertDependency(this.projectsList);
                }
                );
                break;
            case 'user':
                this.userService.getAll().subscribe((res: Response) => {
                    this.users = JSON.parse(res.text());
                    this.selectedTypeItems = this.converter.convertUser(this.users, null);
                }
                );
                break;
            case 'customer':
                this.customersService.getAll().subscribe((res: Response) => {
                    this.customersList = JSON.parse(res.text());
                    this.selectedTypeItems = this.converter.convertCustomers(this.customersList);
                }
                );
                break;
            case 'department':
                this.departmentsService.getAll().subscribe((res: Response) => {
                    this.departmentsList = JSON.parse(res.text());
                    this.selectedTypeItems = this.converter.convertDependency(this.departmentsList);
                }
                );
                break;
        }
    }
    addValidation() {
        this.validation = [];
        this.validation.push({ value: this.model.selectedType, detail: 'Wybierz rodzaj raportu' })
        this.validation.push({ value: this.model.selectedItem, detail: 'Wybierz nazwę raportu' });
        this.validation.push({ value: this.model.fromDate, detail: 'Wypełnij pola daty' });
        this.validation.push({ value: this.model.toDate, detail: 'Wypełnij pola daty' });
    }
    filterRaports() {      
        this.msgs = [];
        this.addValidation();
        this.validation.forEach(function (valid) {
            if(valid.value==null||valid.value=="")
            {
                this.msgs.push({severity:'error', summary:'Błąd', detail:valid.detail});
            }
        }, this);

        if (this.msgs.length > 0) {
            return;
        }

        switch (this.model.selectedType) {
            case 'project':
                this.serchData = { projectId: this.model.selectedItem, dateFrom: this.model.fromDate.toLocaleDateString(), dateTo: this.model.toDate.toLocaleDateString() };
                this.raportService.getByIdProject(this.serchData).subscribe((res: Response) => {
                    this.raportsList = JSON.parse(res.text());
                    this.raportsList.forEach(element => {
                        element.name = element.userLastName + " " + element.userName;
                    });
                }
                );
                break;
            case 'user':
                this.serchData = { userId: this.model.selectedItem, dateFrom: this.model.fromDate.toLocaleDateString(), dateTo: this.model.toDate.toLocaleDateString() };
                this.raportService.getByIdUser(this.serchData).subscribe((res: Response) => {
                    this.raportsList = JSON.parse(res.text());
                    this.raportsList.forEach(element => {
                        element.name = element.userLastName + " " + element.userName;
                    });
                }
                );
                break;
            case 'customer':
                this.serchData = { customerId: this.model.selectedItem, dateFrom: this.model.fromDate.toLocaleDateString(), dateTo: this.model.toDate.toLocaleDateString() };
                this.raportService.getByIdCustomer(this.serchData).subscribe((res: Response) => {
                    this.raportsList = JSON.parse(res.text());
                    this.raportsList.forEach(element => {
                        element.name = element.userLastName + " " + element.userName;
                    });
                }
                );
                break;
            case 'department':
                this.serchData = { teamId: this.model.selectedItem, dateFrom: this.model.fromDate.toLocaleDateString(), dateTo: this.model.toDate.toLocaleDateString() };
                this.raportService.getByIdDepartment(this.serchData).subscribe((res: Response) => {
                    this.raportsList = JSON.parse(res.text());
                    this.raportsList.forEach(element => {
                        element.name = element.userLastName + " " + element.userName;
                    });
                }
                );
                break;
        }
        this.raportService.getDetails(this.serchData).subscribe((res: Response) => {
            var details = JSON.parse(res.text());
            this.model.summary = details.summary;
        });

        

    }
    downloadRaport() {
        var link;
        switch (this.model.selectedType) {
            case 'project':
                link = 'projectId&value=' + this.model.selectedItem + '&to=' + this.model.toDate.getDate() + 'e' + (this.model.toDate.getMonth() + 1) + 'e' + this.model.toDate.getFullYear() + '&from=' + this.model.fromDate.getDate() + 'e' + (this.model.fromDate.getMonth() + 1) + 'e' + (1900 + this.model.fromDate.getYear());
                this.raportService.downloadRaport(link, this.downloadType);
                break;
            case 'user':
                link = 'userId&value=' + this.model.selectedItem + '&to=' + this.model.toDate.getDate() + 'e' + (this.model.toDate.getMonth() + 1) + 'e' + this.model.toDate.getFullYear() + '&from=' + this.model.fromDate.getDate() + 'e' + (this.model.fromDate.getMonth() + 1) + 'e' + (1900 + this.model.fromDate.getYear());
                this.raportService.downloadRaport(link, this.downloadType);
                break;
            case 'customer':
                link = 'customerId&value=' + this.model.selectedItem + '&to=' + this.model.toDate.getDate() + 'e' + (this.model.toDate.getMonth() + 1) + 'e' + this.model.toDate.getFullYear() + '&from=' + this.model.fromDate.getDate() + 'e' + (this.model.fromDate.getMonth() + 1) + 'e' + (1900 + this.model.fromDate.getYear());
                this.raportService.downloadRaport(link, this.downloadType);
                break;
            case 'department':
                link = 'teamId&value=' + this.model.selectedItem + '&to=' + this.model.toDate.getDate() + 'e' + (this.model.toDate.getMonth() + 1) + 'e' + this.model.toDate.getFullYear() + '&from=' + this.model.fromDate.getDate() + 'e' + (this.model.fromDate.getMonth() + 1) + 'e' + (1900 + this.model.fromDate.getYear());
                this.raportService.downloadRaport(link, this.downloadType);
                break;
        }
    }
    rowStyles(rowData: any, rowIndex: number): string {
        let style: string;
        if (rowData.active == '0') {
            style = 'gray';
        }
        return style;
    }
}