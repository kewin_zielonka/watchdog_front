import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { User} from '../_models/index';

@Injectable()
export class AuthGuard implements CanActivate {
    
    constructor(private router: Router) { }
    
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {


        if (localStorage.getItem('currentUser')) {
            let   currentUser : User;
        currentUser = JSON.parse(localStorage.getItem('currentUser'));  
        let roles = route.data["roles"] as Array<string>;

        if (currentUser.role.length==0)
        {
            return true;
        }
        else
        {
            for (let role of currentUser.role) {

                if (roles == null || roles.indexOf(role.role) != -1)
                    return true;
            }
            return false ;
        }
        }
        
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}