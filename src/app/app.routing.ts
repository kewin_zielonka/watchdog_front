import { Routes, RouterModule } from '@angular/router';
 
import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { UserManagmentComponent } from './userManagment/index';
import { RolesComponent } from './roles/index';
import { CustomersComponent } from './customers/index';
import { DepartmentsComponent } from './departments/index';
import { ProjectComponent } from './projects/index';
import { RaportComponent } from './raports/index';
import { ProjectManagmentComponent } from './projectsManagment/index';
import { UserProfile } from './user-profile/user-profile.component';
import { DynamicTableGenerator } from './projectsManagment/dynamic-table-generator/dynamic-table-generator.component';

import { RolesEnum } from './_models/index';


import { AuthGuard } from './_guards/index';
 
const appRoutes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'userManagment', component: UserManagmentComponent , canActivate: [AuthGuard],data: { roles: [RolesEnum.ADMINISTRATOR,RolesEnum.SUPERVISOR,RolesEnum.PROJECT_MANAGER,RolesEnum.TEAM_LEADER] } },                                               
    { path: 'roles', component: RolesComponent , canActivate: [AuthGuard] ,data: { roles: [RolesEnum.ADMINISTRATOR,RolesEnum.SUPERVISOR] }},
    { path: 'customers', component: CustomersComponent , canActivate: [AuthGuard],data: { roles: [RolesEnum.ADMINISTRATOR,RolesEnum.SUPERVISOR,RolesEnum.PROJECT_MANAGER,RolesEnum.TEAM_MEMBER,RolesEnum.TEAM_LEADER] } },                                               
    { path: 'departments', component: DepartmentsComponent , canActivate: [AuthGuard],data: { roles: [RolesEnum.ADMINISTRATOR,RolesEnum.SUPERVISOR,RolesEnum.PROJECT_MANAGER,RolesEnum.TEAM_MEMBER,RolesEnum.TEAM_LEADER,RolesEnum.CUSTOMER] } },                                               
    { path: 'projects', component: ProjectComponent , canActivate: [AuthGuard],data: { roles:  [RolesEnum.SUPERVISOR,RolesEnum.PROJECT_MANAGER,RolesEnum.TEAM_MEMBER,RolesEnum.TEAM_LEADER,RolesEnum.CUSTOMER]} },                                               
    { path: 'projectsManagment', component: ProjectManagmentComponent , canActivate: [AuthGuard],data: { roles: [RolesEnum.SUPERVISOR,RolesEnum.PROJECT_MANAGER,RolesEnum.TEAM_MEMBER,RolesEnum.TEAM_LEADER,RolesEnum.CUSTOMER] } },                                               
    { path: 'raports', component: RaportComponent , canActivate: [AuthGuard],data: { roles: [RolesEnum.SUPERVISOR,RolesEnum.PROJECT_MANAGER,RolesEnum.TEAM_MEMBER,RolesEnum.TEAM_LEADER,RolesEnum.CUSTOMER] } },    
    { path: 'userProfile', component: UserProfile , canActivate: [AuthGuard] },    
    { path: 'projectsManagment/dynamicTableGenerator', component: DynamicTableGenerator , canActivate: [AuthGuard],data: { roles: [RolesEnum.SUPERVISOR,RolesEnum.PROJECT_MANAGER,RolesEnum.TEAM_MEMBER,RolesEnum.TEAM_LEADER,RolesEnum.CUSTOMER] } },                                     

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];
 
export const routing = RouterModule.forRoot(appRoutes);