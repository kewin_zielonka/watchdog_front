﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Project,Task,User,WorkHoursSerch } from '../_models/index';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';

@Injectable()
export class ProjectService {
    //fake
    projects:Project[];
user: User ;
private url = environment.url;  // URL to web api

constructor(private http: Http) { }

getAll() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers();
    headers.append('Content-Type', 'text/plain');
    return this.http.post(this.url+'/rest/getUserProjects', this.user.id,{withCredentials: true,headers:headers});
}
getById(id: string)   
{      
    let headers = new Headers();
    headers.append('Content-Type', 'text/plain');
    return this.http.post(this.url+'/rest/getWorkHour', id,{withCredentials: true,headers:headers})    
}
deleteTask(id: string)   
{      
    let headers = new Headers();
    headers.append('Content-Type', 'text/plain');
    return this.http.post(this.url+'/rest/delWorkHour', id,{withCredentials: true,headers:headers})    
}
getUserWorkHours(workHoursSerch:WorkHoursSerch)   
{      
    let headers = new Headers();
    headers.append('Content-Type', 'text/plain');
    return this.http.post(this.url+'/rest/getUserWorkHours', workHoursSerch,{withCredentials: true,headers:headers})    
}
saveTask(task:Object)
{
    let headers = new Headers();
    headers.append('Content-Type', 'text/plain');
    return this.http.post(this.url+'/rest/saveWorkHour', task,{withCredentials: true,headers:headers})
}
private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
}

}