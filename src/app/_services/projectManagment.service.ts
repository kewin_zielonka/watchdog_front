﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Project,TeamToSave } from '../_models/index';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';

@Injectable()
export class ProjectManagmentService {
  //fake
    projects:Project[];
    
    private url = environment.url;  // URL to web api
    
    constructor(private http: Http) { }
    
    getAll() {
        return this.http.get(this.url+'/rest/projects',{withCredentials: true})

    }
    
    getById(id: string)   
    {
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/getProject', id,{withCredentials: true,headers:headers})    
        }
    
    create(project: Project) {     
        project.isActive='0';
       let headers = new Headers();
           headers.append('Content-Type', 'text/plain');
           return this.http.post(this.url+'/rest/addProject', project,{withCredentials: true,headers:headers})
           
    }
    
    update(project: Project) {
         let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/updateProject', project,{withCredentials: true,headers:headers})
       
    }
    updateStatus(project: Project) {
        
              let headers = new Headers();
             headers.append('Content-Type', 'text/plain');
             return this.http.post(this.url+'/rest/updateProjectStatus', {id:project.id,isActive:project.isActive},{withCredentials: true,headers:headers})
            
         }
    updateStatusInProject(userData: Object) {
        let headers = new Headers();
       headers.append('Content-Type', 'text/plain');
       return this.http.post(this.url+'/rest/activeUserProject',userData,{withCredentials: true,headers:headers})
      
   }
    delete(id: string) {
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/delProject', id,{withCredentials: true,headers:headers}) 
       
        }
    saveTeam(teamToSave:TeamToSave)
    {
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/addUsersToProject', teamToSave,{withCredentials: true,headers:headers})

    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
    
}