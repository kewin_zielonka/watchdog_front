﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Department ,TeamToSave} from '../_models/index';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';

@Injectable()
export class DepartmentsService {
    private url = environment.url;  // URL to web api
    usersId:String[]

//fake
   // departments:Department[];

    constructor(private http: Http) {

    }
    getById(id: string)   
    {/*
        for (const customer of this.departments) { 
            if(id==customer.name)
            {
                return customer;

                //localStorage.setItem('Customers', JSON.stringify( this.customers));

            }
        }
        return null;
*/
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/getDepartment', id,{withCredentials: true,headers:headers})
        
    }
    getAll() {
     /*   this.departments = JSON.parse(localStorage.getItem('Departments')) || [];
      return this.departments;
*/
      return this.http.get(this.url+'/rest/departments',{withCredentials: true})
    }
    saveTeam(teamToSave:TeamToSave)
    {
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/addUsersToDepartment', teamToSave,{withCredentials: true,headers:headers})

    }
    
    create(department: Department) {
      /*  this.departments.push(department)
        localStorage.setItem('Departments', JSON.stringify( this.departments));*/
       let headers = new Headers();
           headers.append('Content-Type', 'text/plain');
           return this.http.post(this.url+'/rest/addDepartment', department,{withCredentials: true,headers:headers})

    }
    
    update(department: Department) {
       
        
        
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/updateDepartment', department,{withCredentials: true,headers:headers})
        
    }
    
    delete(id: string) {
       /* for (const department of this.departments) { 
            if(id==department.name)
            {
                var index = this.departments.indexOf(department, 0);
                this.departments.splice(index, 1);
                localStorage.setItem('Departments', JSON.stringify( this.departments));

            }
        }*/
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/delDepartment', id,{withCredentials: true,headers:headers}) 
        }
       
   
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
    
}