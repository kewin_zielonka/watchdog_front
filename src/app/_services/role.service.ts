﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { User,Role } from '../_models/index';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';


@Injectable()
export class RoleService {
    private url = environment.url;  // URL to web api
    curentUser:User;
    
    constructor(private http: Http) { }
    getById(id: string)   
    {
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/getRole', id,{withCredentials: true,headers:headers})
    }
    getAll() {
        return this.http.get(this.url+'/rest/roles',{withCredentials: true})
    }
    
    
    create(role: Role) {
        
        let headers = new Headers();
           headers.append('Content-Type', 'text/plain');
           return this.http.post(this.url+'/rest/addRole', role,{withCredentials: true,headers:headers})

    }
    
    update(role: Role) {
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/updateRole', role,{withCredentials: true,headers:headers})
    }
    
    delete(id: string) {
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/delRole', id,{withCredentials: true,headers:headers})    }
       
   
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
    
}