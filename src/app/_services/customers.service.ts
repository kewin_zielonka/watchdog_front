﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Customer } from '../_models/index';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';

@Injectable()
export class CustomersService {
    private url =environment.url;  // URL to web api


    constructor(private http: Http) {

    }
    getById(id: string)   
    {
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/getCustomer', id,{withCredentials: true,headers:headers})
        
    }
    getAll() {
      return this.http.get(this.url+'/rest/customers',{withCredentials: true})
    }
    
    
    create(customer: Customer) {

       let headers = new Headers();
           headers.append('Content-Type', 'text/plain');
           return this.http.post(this.url+'/rest/addCustomer', customer,{withCredentials: true,headers:headers})

    }
    
    update(customer: Customer) {
        
        
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/updateCustomer', customer,{withCredentials: true,headers:headers})
        
    }
    
    delete(id: string) {

        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/delCustomer', id,{withCredentials: true,headers:headers}) 
        }
       
   
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
    
}