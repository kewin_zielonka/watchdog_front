﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { User,RoleToUser } from '../_models/index';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';

@Injectable()
export class UserService {
    private url =environment.url;  // URL to web api
    curentUser:User;
    
    constructor(private http: Http) { }
    
    getAll() {
        return this.http.get(this.url+'/rest/users',{withCredentials: true})
    }
    
    getById(id: string)   
    {
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/getUser', id,{withCredentials: true,headers:headers})    }
    
    create(user: User) {     
        let headers = new Headers();
           headers.append('Content-Type', 'text/plain');
           return this.http.post(this.url+'/rest/addUser', user,{withCredentials: true,headers:headers})
    }
    
    update(user: User) {
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/updateUser', user,{withCredentials: true,headers:headers})
    }
    
    delete(id: string) {
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/delUser', id,{withCredentials: true,headers:headers})    
        }
    addRoleToUser(obj:RoleToUser) {
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/addRoleToUser', obj,{withCredentials: true,headers:headers})    
        }
   removeRoleFromUser(obj:RoleToUser) {
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/delRoleFromUser', obj,{withCredentials: true,headers:headers})    
        }
   changeUserPassword(obj:Object) {
       let headers = new Headers();
       headers.append('Content-Type', 'text/plain');
       return this.http.post(this.url+'/rest/newPassword', obj,{withCredentials: true,headers:headers})                    
       }

    checkMatchesWithEncodedPassword(user: User){
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/checkMatchesWithEncodedPassword', user,{withCredentials: true,headers:headers})
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
    
}