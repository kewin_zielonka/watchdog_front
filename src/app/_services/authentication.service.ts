﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { User } from '../_models/index';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthenticationService {
    currentUser: User;


    constructor(private http: Http) { }
    private url = environment.url;  // URL to web api
    login(username: string, password: string) {

        let headers = new Headers();

        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        headers.append('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8');
        var search = new URLSearchParams()
        search.set('username', username),
            search.set('password', password);



        return this.http.post(this.url + '/login', search, { withCredentials: true, headers: headers })
            .map((response: Response) => {

                //   localStorage.setItem('currentUser', username);


            });
    }

    logout() {
        // remove user from local storage to log user out
        this.http.get(this.url + '/logout', { withCredentials: true })
            .map((response: Response) => {
            });
        localStorage.removeItem('currentUser');

        return this.http.get(this.url + '/logout', { withCredentials: true })
            .map((response: Response) => {
            });

    }

    isLoggedin() {
        if (localStorage.getItem('currentUser') == null) {
            return false;
        }
        else {
            return true;
        }

    }

    isSession() {
        
    }
}
