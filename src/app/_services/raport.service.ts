﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { DownloadType } from '../_models/index';

import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';

@Injectable()
export class RaportService {

    private url = environment.url;  // URL to web api

    constructor(private http: Http) { }
    getByIdProject(filterData: Object)   
    {      
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/projectReport', filterData,{withCredentials: true,headers:headers})    
    }
    getByIdUser(filterData: Object)   
    {      
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/getUserWorkHours', filterData,{withCredentials: true,headers:headers})    
    }
    getDetails(filterData: Object)   
    {
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/getDetails', filterData,{withCredentials: true,headers:headers})    
    }
    getByIdDepartment(filterData: Object)   
    {      
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest//departmentReport', filterData,{withCredentials: true,headers:headers})    
    }
    getByIdCustomer(filterData: Object)   
    {      
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest//customerReport', filterData,{withCredentials: true,headers:headers})    
    }
    changeTaskStatus(id: string)   
    {      
        let headers = new Headers();
        headers.append('Content-Type', 'text/plain');
        return this.http.post(this.url+'/rest/activeWorkHour', id,{withCredentials: true,headers:headers})    
    }

    checkIfRaportsAvailable(link: string){
        let headers = new Headers();
        
        headers.append('Content-Type', 'text/plain');
        return this.http.get(this.url+'/rest/checkIfRaportsAvailable' + '?type='+link,{withCredentials: true,headers:headers}) 
    }

    downloadRaport(link: string, downloadType: DownloadType)   
    {   
        var newWindow = window.open(this.url+'/rest/build'+ downloadType+'?type='+link);

    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}