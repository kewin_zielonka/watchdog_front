import { Injectable, Inject } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { User} from '../_models/index';

@Injectable()
export class PermissionsEngine {
    private userPermissions: Array<String> = Array<String>();
private permissionsString = "";

constructor()  {
    
    this.calcPermissions();
}

calcPermissions() {
    this.permissionsString = localStorage.getItem("permissions");
    if ( this.permissionsString ) {
        this.userPermissions = this.permissionsString.split(",");
    } else {
        this.userPermissions = new Array<String>();
    }
}

hasPermission( _slug: Array<String> ) {
    if (localStorage.getItem('currentUser')) {
        let   currentUser : User;
    currentUser = JSON.parse(localStorage.getItem('currentUser'));  
    for (let role of currentUser.role) {
        if (_slug == null || _slug.indexOf(role.role) != -1 )
            return true;
    }
    return false ;
    }
}
}