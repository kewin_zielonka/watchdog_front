﻿import { Component, OnInit, Renderer, ElementRef } from '@angular/core';

import { User, Role, RoleToUser, Validation, RolesEnum } from '../_models/index';

import { UserService, RoleService, PermissionsEngine } from '../_services/index';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { InputTextModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { PickListModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { Message } from 'primeng/primeng';
import {BlockUIModule} from 'primeng/primeng';
import {PanelModule} from 'primeng/primeng';
import {TranslateService} from '@ngx-translate/core';

@Component({
    moduleId: module.id,
    templateUrl: 'userManagment.component.html',
})

export class UserManagmentComponent implements OnInit {
    model: any = {};
    inputDisable: any = {};
    currentUser: User;
    user: User
    users: User[];
    roles: Role[];
    userRoles: Role[];
    selectedUser: User;
    editPasswordPopup: boolean = false;
    editRolesPopup: boolean = false;
    display: boolean = false;
    create: boolean = true;
    picketLinkVisible: string = 'none';
    msgs: Message[] = [];
    validation: Validation[] = [];
    cols: any[];
    rolesEnum = RolesEnum;
    numberOfAllRoles: number;
    showNoUserRolesWarning: boolean = false;

    constructor(private userService: UserService, private roleService: RoleService, el: ElementRef, renderer: Renderer, private _permissionsEngine: PermissionsEngine,
    ) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (this._permissionsEngine.hasPermission([this.rolesEnum.SUPERVISOR.toString(),this.rolesEnum.ADMINISTRATOR.toString()])) {
            this.loadAllUsers();
        }
        else {
            this.loadUserData(this.currentUser.username);
        }
        this.loadAllRoles()      
        this.userRoles = [];
        this.addValidation();       
    }

    ngOnInit() {

    }
    clearUserUI() {
        this.display = false;
        this.model = {};
        this.create = true;
        this.inputDisable = {};
        document.getElementById("password").style.visibility = "visible";
        document.getElementById("subpassword").style.visibility = "visible";
    }

    displayButtonAction() {
        this.addValidation();
        this.validation.forEach(function (valid) {
            if (valid.value == null || valid.value == "") {
                this.msgs.push({ severity: 'error', summary: 'Błąd', detail: valid.detail });
            }
        }, this);
        if (this.msgs.length == 0) {
            if (this.model.password == this.model.subpassword) {
                if (this.create) {
                    this.userService.create(this.model).subscribe((res: Response) => {
                        this.loadAllUsers();
                    });
                }
                else {
                    this.userService.update(this.model).subscribe((res: Response) => {
                        this.loadAllUsers();
                    });
                }
                this.clearUserUI();

            }
            else {
                this.msgs.push({ severity: 'error', summary: 'Błąd', detail: "Hasła różnią się" });
            }
        }
    }

    showDialog() {
        this.clearUserUI();
        this.display = true;
    }
    editPassword(id: string) {
        this.editPasswordPopup = true;
        this.model.id = id;
    }
    editRoles() {
        this.editRolesPopup = true;
    }
    private loadAllUsers() {
        this.userService.getAll().subscribe((res: Response) => {
            this.users = JSON.parse(res.text());
        }
        );
    }
    private loadUserData(username: string) {
        this.userService.getById(username).subscribe((res: Response) => {
            var currentUserData = JSON.parse(res.text());
            this.users = [currentUserData];
        });
    }
    addValidation() {
        this.validation = [];
        this.validation.push({ value: this.model.firstName, detail: 'Wypełnij pole imie' });
        this.validation.push({ value: this.model.lastName, detail: 'Wypełnij pole nazwiska' });
        this.validation.push({ value: this.model.email, detail: 'Wypełnij pole email' });
        this.validation.push({ value: this.model.username, detail: 'Wypełnij pole login' });
        if (this.create) {
            this.validation.push({ value: this.model.password, detail: 'Wypełnij pole hasła' });
            this.validation.push({ value: this.model.subpassword, detail: 'Potwierdź hasło' });
        }
    }
    showSuccess() {
        this.msgs = [];
        this.msgs.push({ severity: 'success', summary: 'Udało się', detail: 'Zmieniłeś hasło!' });
    }
    showFailure() {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: 'Błąd', detail: 'Wpisz 2 razy to samo hasło!!' });
    }
    createUserUI(user: User) {
        this.model.username = user.username;
        this.model.firstName = user.firstName;
        this.model.lastName = user.lastName;
        this.model.email = user.email;
        this.inputDisable.username = true;
        this.inputDisable.password = true;
        this.inputDisable.subpassword = true;

        this.display = true;
    }
    editUser(username: string) {
        document.getElementById("password").style.visibility = "hidden";
        document.getElementById("subpassword").style.visibility = "hidden";
        this.userService.getById(username).subscribe((res: Response) => {
            this.user = JSON.parse(res.text());
            this.createUserUI(this.user);
            this.create = false;
        }
        );
    }
    editUserPassword() {
        if (this.model.password != null && this.model.password != "" && this.model.password == this.model.subpassword) {
            this.userService.changeUserPassword({ id: this.model.id, password: this.model.password }).subscribe((res: Response) => {
                this.clearPasswordUI();
                this.showSuccess();
            }
            );
        }
        else {
            this.showFailure()
        }
    }
    deleteUser(username: string) {
        this.userService.delete(username).subscribe(() => { this.loadAllUsers() });
    }
    private loadAllRoles() {
        this.roleService.getAll().subscribe((res: Response) => {
            this.roles = JSON.parse(res.text());
            this.numberOfAllRoles = this.roles.length;
        }
        );     
    }
    onRowSelect(user) {     
        this.roleService.getAll().subscribe((res: Response) => {
            this.roles = JSON.parse(res.text());
            this.picketLinkVisible = '';
            this.selectedUser = user;
            this.userRoles = user.role;

            let rolesNotContaintedByUser: Role[] = [];
  
            this.roles.forEach(role => {
                if(!this.userRoles.find(userRole => userRole.id === role.id))
                    rolesNotContaintedByUser.push(role);  
            });              
            
            this.roles = rolesNotContaintedByUser;
            this.editRoles();
        });      
    }
    clearPasswordUI() {
        this.editPasswordPopup = false;
        this.model = {};

    }
    clearRolesUI() {
        this.editRolesPopup = false;
        this.model = {};

    }
    addRoleToUser(event) {
        if(event.items.length != 0){
            let roleToUser = { username: this.selectedUser.username, roleList: event.items };
            this.userService.addRoleToUser(roleToUser).subscribe((res: Response) => {});
        }        
    }
    removeRoleFromUser(event) { 
        if(event.items.length != 0){
            let roleToUser = { username: this.selectedUser.username, roleList: event.items };
            this.userService.removeRoleFromUser(roleToUser).subscribe((res: Response) => {});
            this.checkIfUserHasNoRoles();
        }                
    }
    rowStyles(rowData: any, rowIndex: number): string {
        let style: string;
        if (rowData.active == '0') {
            style = 'gray';
        }
        return style;
    }

    checkIfUserHasNoRoles(){
        if(this.roles.length === this.numberOfAllRoles){
            this.showNoUserRolesWarning = true;
        }
    }

    setShowNoUserRolesWarning(decision: boolean){
        this.showNoUserRolesWarning = decision;
    }
}