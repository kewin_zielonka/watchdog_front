import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService, AuthenticationService } from '../_services/index';
import {ButtonModule} from 'primeng/primeng';
import {MenuModule,MenuItem} from 'primeng/primeng';


@Component({
    moduleId: module.id,
    templateUrl: 'menu.component.html',
    selector: 'menu',

})
 
export class MenuComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    private items: MenuItem[];
    

 
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService) { }
 
    ngOnInit() {
        this.items = [
                      {label: 'Użytkownicy', icon: 'fa-plus', routerLink: ['/userManagment']},
                      {label: 'Kliecni', icon: 'fa-download'},
                      {label: 'Kliecni', icon: 'fa-download'},
                      {label: 'Kliecni', icon: 'fa-download'},
                      {label: 'Kliecni', icon: 'fa-download'},
                      {label: 'Kliecni', icon: 'fa-download'},
                      {label: 'Kliecni', icon: 'fa-download'},
                      {label: 'Kliecni', icon: 'fa-download'},
                      {label: 'Kliecni', icon: 'fa-download'},
                      {label: 'Kliecni', icon: 'fa-download'},
                      {label: 'Kliecni', icon: 'fa-download'},
                      {label: 'Kliecni', icon: 'fa-download'},
                      {label: 'Kliecni', icon: 'fa-download'},
                      {label: 'Kliecni', icon: 'fa-download'},
                      {label: 'Projekty', icon: 'fa-refresh'}
                  ];       
        this.authenticationService.logout();
 
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }
 
    login() {
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}