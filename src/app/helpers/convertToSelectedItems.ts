import { Injectable } from '@angular/core';
import {SelectItem} from 'primeng/primeng';
import { RolesEnum,User,Department,Customer,Project } from '../_models/index';


@Injectable()
export class ConvertToSelectedItems{
    selectedItems: SelectItem[];

constructor() { }

convertUser( users:Array<User>,roleEnum:RolesEnum) {    
    this.selectedItems = [];
    this.selectedItems.push({label:'Wybierz', value:null});
    
    for (const user of users) { 
        var added = false;
        for (const role of user.role) {
            if(!added)
            {
                if (roleEnum==null||role.role == roleEnum)
                {
                    if(user.active=="1")
                    {
                        this.selectedItems.push({label:user.firstName + " "+ user.lastName, value:user.id});
                        added = true;
                    }
                }
            }
        }
    }
    return this.selectedItems
}
convertDependency( departments:Array<Department>) {
    this.selectedItems = [];
    this.selectedItems.push({label:'Wybierz', value:null});
    for (const department of departments) { 
        this.selectedItems.push({label:department.name , value:department.id});
    }            
    return this.selectedItems
}
convertCustomers( customers:Array<Customer>) {
    this.selectedItems = [];
    this.selectedItems.push({label:'Wybierz', value:null});
    for (const customer of customers) { 
        this.selectedItems.push({label:customer.name, value:customer.id});
    }            
    return this.selectedItems
}
convertProjects( projects:Array<Project>,all:boolean) {
    this.selectedItems = [];
    this.selectedItems.push({label:'Wybierz', value:null});
    for (const project of projects) { 
        if(all)
        {
            this.selectedItems.push({label:project.name , value:project.id});
        }else
        {
            if(project.isActive=='1'&&project.isProjectActive=='1')
            {
                this.selectedItems.push({label:project.name , value:project.id});
            }
        }
    }            
    return this.selectedItems
}
}