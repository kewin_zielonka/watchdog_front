﻿import { Component, OnInit, Renderer, ElementRef } from '@angular/core';

import { Department, User, TeamToSave, Validation } from '../_models/index';
import { DepartmentsService, UserService, PermissionsEngine } from '../_services/index';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { InputTextModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { InputTextareaModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { SelectItem } from 'primeng/primeng';
import { RolesEnum } from '../_models/index';
import { ConvertToSelectedItems } from '../helpers/index';
import { GrowlModule } from 'primeng/primeng';
import { Message } from 'primeng/primeng';
import {ConfirmDialogModule,ConfirmationService} from 'primeng/primeng';


@Component({
    moduleId: module.id,
    templateUrl: 'departments.component.html',
})

export class DepartmentsComponent implements OnInit {
    model: any = {};
    inputDisable: any = {};
    teamLeaders: SelectItem[];
    users: User[];
    usersInTeam: User[] = [];
    msgs: Message[] = [];
    validation: Validation[] = [];

    departments: Department[];
    department: Department;
    display: boolean = false;
    team: boolean;
    create: boolean = true;
    hero: string;

    selectedCity: string;
    rolesEnum = RolesEnum;

    constructor(private _permissionsEngine: PermissionsEngine, private userService: UserService, private departmentsService: DepartmentsService, el: ElementRef, renderer: Renderer, private converter: ConvertToSelectedItems, private confirmationService: ConfirmationService) {
        this.loadTeamLeaders();
        this.loadAllDepartments();

    }

    ngOnInit() {
    }
    clearDepartmentUI() {
        this.display = false;
        this.model = {};
        this.create = true;
        this.inputDisable = {};

    }
    createDepartmentUI(department: Department) {
        this.create = false;
        this.model.id = department.id;
        this.model.name = department.name;
        this.model.leader = department.leader.id;
        this.inputDisable.name = true;
        this.showDialog();
    }
    displayButtonAction() {
        this.msgs = [];
        this.addValidation();
        this.validation.forEach(function (valid) {
            if (valid.value == null || valid.value == "") {
                this.msgs.push({ severity: 'error', summary: 'Błąd', detail: valid.detail });
            }
        }, this);
        if (this.msgs.length == 0) {
            if (this.create) {
                this.departmentsService.create(this.model).subscribe((res: Response) => {
                    this.loadAllDepartments();
                });

            }
            else {
                this.departmentsService.update(this.model).subscribe((res: Response) => {
                    this.loadAllDepartments();
                });
            }
            this.clearDepartmentUI();

        }

    }
    addValidation() {
        this.validation = [];
        this.validation.push({ value: this.model.name, detail: 'Wypełnij pole nazwy' });
        this.validation.push({ value: this.model.leader, detail: 'Wypełnij pole lidera' });
    }
    close() {
        this.team = false;
    }
    saveTeam() {
        let teamToSave = { id: this.department.id, users: [] };
        teamToSave.users = [];

        for (const teamUser of this.usersInTeam) {
            teamToSave.users.push(teamUser.id)
        }
        this.departmentsService.saveTeam(teamToSave).subscribe(() => { this.loadAllDepartments() });
        this.team = false;
    }
    deleteDepartment(department: string) {
        var response = this.departmentsService.delete(department);
        response.subscribe((res: Response) => {
            if (res.text() == "false") {
                this.msgs.push({ severity: 'error', summary: 'Błąd', detail: "Nie można usunąć działu, dział ma powiązania" });
            }
            else {
                this.msgs.push({ severity: 'success', summary: 'Udało się', detail: 'Dział został usunięty' });
            }
            this.loadAllDepartments();
        });

    }
    showDialog() {
        this.display = true;
    }
    editDepartment(customer: string) {
        this.departmentsService.getById(customer).subscribe((res: Response) => {
            this.department = JSON.parse(res.text());

            this.createDepartmentUI(this.department);
        }
        );

    }
    private loadAllDepartments() {
        this.departmentsService.getAll().subscribe((res: Response) => {
            this.departments = JSON.parse(res.text());
        }

        );

    }
    private loadTeamLeaders() {

        this.userService.getAll().subscribe((res: Response) => {
            this.users = JSON.parse(res.text());
            this.teamLeaders = this.converter.convertUser(this.users, RolesEnum.TEAM_LEADER);
        }
        );
    }
    private addUsersToTeam(department: Department) {

        this.team = true;
        this.userService.getAll().subscribe((res: Response) => {
            var usersFromDB = JSON.parse(res.text());
            this.users = [];
            for (const user of usersFromDB) {
                if (user.active == "1") {
                    this.users.push(user);
                }
            }
            this.department = department;
            this.usersInTeam = department.users;
            for (const teamUser of this.usersInTeam) {
                for (const user of this.users) {
                    if (user.id == teamUser.id) {
                        var index = this.users.indexOf(user, 0);
                        this.users.splice(index, 1);
                    }
                }
            }
        }
        );
    }

    showDeleteDepartmentConfirm(department: string){
        this.confirmationService.confirm({
            message: 'Czy napewno chcesz usunąć dział?',
            header: 'Wymagane potwierdzenie',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.deleteDepartment(department);
            },
            reject: () => {
                this.msgs = [{severity:'warn', summary:'Brak potwierdzenia', detail:'Nie zezwolono na usunięcie'}];
            }
        });
    }

}