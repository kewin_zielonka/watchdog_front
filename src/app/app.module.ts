import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';

import { MessagesModule } from 'primeng/primeng';
import { InputTextModule } from 'primeng/primeng';
import { PasswordModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import {MenuModule,MenuItem} from 'primeng/primeng';
import {InputTextareaModule} from 'primeng/primeng';
import {PickListModule} from 'primeng/primeng';
import {DropdownModule} from 'primeng/primeng';
import {TooltipModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';
import {CheckboxModule} from 'primeng/primeng';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import { SplitButtonModule } from 'primeng/primeng';
import {ConfirmDialogModule,ConfirmationService} from 'primeng/primeng';
import {FieldsetModule} from 'primeng/primeng';
import {PanelModule} from 'primeng/primeng';
import {ToolbarModule} from 'primeng/primeng';
import {DataListModule} from 'primeng/primeng';
import {OverlayPanelModule} from 'primeng/primeng';
import { StepsModule } from 'primeng/primeng';
import {TabViewModule} from 'primeng/primeng';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { AlertComponent } from './_directives/index';
import { AuthGuard } from './_guards/index';
import { AlertService, AuthenticationService, UserService ,RoleService,PermissionsEngine,CustomersService,DepartmentsService,RaportService,ProjectService,ProjectManagmentService } from './_services/index';
import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { MenuComponent } from './menu/index';
import { UserManagmentComponent } from './userManagment/index';
import { RolesComponent } from './roles/index';
import { CustomersComponent } from './customers/index';
import { DepartmentsComponent } from './departments/index';
import { ProjectComponent } from './projects/index';
import { RaportComponent } from './raports/index';
import { ProjectManagmentComponent } from './projectsManagment/index';
import { ConvertToSelectedItems } from './helpers/index';
import { UserProfile } from './user-profile/user-profile.component';
import { UserBasicInformation } from './user-profile/user-basic-information/user-basic-information.component';
import { UserProjects } from './user-profile/user-projects/user-projects.component';
import { UserRoles } from './user-profile/user-roles/user-roles.component';
import { UserRoleDescription } from './user-profile/user-roles/user-role-description/user-role-description.component';
import { UserChangePassword } from './user-profile/user-basic-information/user-change-password/user-change-password.component';
import { UserProjectTeamDescription } from './user-profile/user-projects/user-project-team-description/user-project-team-description.component';
import { UserProjectsRaport } from './user-profile/user-projects/user-projects-raport/user-projects-raport.component';
import { DynamicTableGenerator } from './projectsManagment/dynamic-table-generator/dynamic-table-generator.component';
import { ProjectBasicInformation } from './projectsManagment/dynamic-table-generator/project-basic-information/project-basic-information.component';
import { RaportTableSchemat } from './projectsManagment/dynamic-table-generator/raport-table-schemat/raport-table-schemat.component';

export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    imports: [
        TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: (createTranslateLoader),
            deps: [Http]
            }
        }), 
        TabViewModule,   
        StepsModule,  
        OverlayPanelModule,
        DataListModule,
        ToolbarModule,
        PanelModule,
        FieldsetModule,
        ConfirmDialogModule,
        SplitButtonModule,
        ButtonModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        DataTableModule,
        MessagesModule,
        MenuModule,
        InputTextModule,
        PasswordModule,
        ReactiveFormsModule,
        SharedModule,
        DialogModule,
        InputTextareaModule,
        PickListModule,
        DropdownModule,
        TooltipModule,
        CalendarModule,
        CheckboxModule,
        GrowlModule,
        routing
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        UserManagmentComponent,
        MenuComponent,
        RolesComponent,
        CustomersComponent,
        DepartmentsComponent,
        ProjectComponent,
        ProjectManagmentComponent,
        RaportComponent,
        UserProfile,
        UserBasicInformation,
        UserProjects,
        UserRoles,
        UserRoleDescription,
        UserChangePassword,
        UserProjectsRaport,
        UserProjectTeamDescription,
        DynamicTableGenerator,
        ProjectBasicInformation,
        RaportTableSchemat
    ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        RoleService,
        PermissionsEngine,
        CustomersService,
        DepartmentsService,
        ProjectService,
        RaportService,
        ProjectManagmentService,
        ConvertToSelectedItems,
        ConfirmationService    
    ],
    bootstrap: [AppComponent]
})
 
export class AppModule { }