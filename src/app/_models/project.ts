﻿import {User,Department,Customer } from '../_models/index';

export class Project {
    id:string
    name:string
    customer: Customer;
    departmentCustomer: string;
    projectManagerCustomer: User;
    accountant:User
    department:Department
    projectManager:User
    isActive:string
    team:any[];
    isProjectActive:string

}