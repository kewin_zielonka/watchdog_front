import { Project } from './project'

export class ProjectUrlParameters{
    private _projectName: string;
    private _customerName: string;
    private _departmentName: string; 
    private _projectManagerFirstAndLastName: string

    copySelectedInformation(project: Project){
        this._projectName = project.name;
        this._customerName = project.customer.name;
        this._departmentName = project.department.name;
        this._projectManagerFirstAndLastName = project.projectManager.firstName 
                                            + project.projectManager.lastName;
    }

    set projectName(projectName: string){
        this._projectName = projectName;
    }

    get projectName(){
        return this._projectName;
    }

    set customerName(customerName: string){
        this._customerName = customerName;
    }

    get customerName(){
        return this._customerName;
    }

    set departmentName(departmentName: string){
        this._departmentName = departmentName;
    }

    get departmentName(){
        return this._departmentName;
    }

    set projectManagerFirstAndLastName(projectManagerFirstAndLastName: string){
        this._projectManagerFirstAndLastName = projectManagerFirstAndLastName;
    }

    get projectManagerFirstAndLastName(){
        return this._projectManagerFirstAndLastName;
    }
}