﻿import {User } from '../_models/index';

export class Department {
    id:string
    name:string
    leader:User
    users:any[];

}