﻿export class Customer {
    id:string
    name:string
    city:string
    country:string
    street:string
    houseNumber:string
    apartmentNumber:string
    zipCode:string
    address:string
}