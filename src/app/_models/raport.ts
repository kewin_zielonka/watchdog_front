﻿export class Raport {
    userId:string
    projectId:string
    description: string;
    fromH: string;
    toH: string;
    hours: string;
    projectName:string
    userLastName:string
    userName:string
    date:string;
    id:string;  
    name:string = this.userLastName + " " + this.userName;
}