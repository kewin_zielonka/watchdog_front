
export enum RaportType {
    PROJECT = <any> 'PROJECT',
    USER = <any> 'USER',
    CUSTOMER = <any> 'CUSTOMER',
    DEPARTMENT = <any> 'DEPARTMENT'
}