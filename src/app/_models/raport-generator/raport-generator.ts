import { FileGenerator } from './file-generator'
import { DownloadType } from '../downloadType'
import { RaportType } from './raport-type'
import { RaportService } from '../../_services/'
import { Response } from '@angular/http';

export class RaportGenerator implements FileGenerator {
    private _selectedId: string;
    private _fromDate: Date;
    private _toDate: Date;
    private _downloadLink: string;
    private _downloadType: DownloadType;
    private _raportType: RaportType;
    private _raportService: RaportService;

    public generateRaport(): void {
        this._raportService.checkIfRaportsAvailable(this._downloadLink).subscribe((res: Response) => {
            let allowDownload = JSON.parse(res.text());
            if(allowDownload)
                 this._raportService.downloadRaport(this._downloadLink, this._downloadType);
        });   
    }   

    public generateDownloadLink(): string {
        this._downloadLink = RaportType[RaportType[this._raportType]].toLowerCase() + 
                             'Id&value=' + this._selectedId + 
                             '&to=' + this._toDate.getDate() + 
                             'e' + (this._toDate.getMonth() + 1) + 
                             'e' + this._toDate.getFullYear() + 
                             '&from=' + this._fromDate.getDate() + 
                             'e' + (this._fromDate.getMonth() + 1) + 
                             'e' + (this._fromDate.getFullYear());
        return this._downloadLink;
    }

    set selectedId(selectedId: string) {
        this._selectedId = selectedId;
    }

    get selectedId(): string {
        return this._selectedId;
    }

    set fromDate(fromDate: Date) {
        this._fromDate = fromDate;
    }

    get fromDate(): Date {
        return this._fromDate;
    }

    set toDate(toDate: Date) {
        this._toDate = toDate;
    }

    get toDate(): Date {
        return this._toDate;
    }

    set downloadLink(downloadLink: string) {
        this._downloadLink = downloadLink;
    }

    get downloadLink(): string {
        return this._downloadLink;
    }

    set downloadType(downloadType: DownloadType) {
        this._downloadType = downloadType;
    }

    get downloadType(): DownloadType {
        return this._downloadType;
    }

    set raportType(raportType: RaportType) {
        this._raportType = raportType;
    }

    get raportType(): RaportType {
        return this._raportType;
    }

    set raportService(raportService: RaportService) {
        this._raportService = raportService;
    }
}