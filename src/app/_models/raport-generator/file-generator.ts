import { DownloadType } from '../downloadType'

export interface FileGenerator {
    generateRaport(): void;
    generateDownloadLink(): string;
}