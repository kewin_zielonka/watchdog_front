﻿export class Task {
    userId:string
    projectId:string
    description: string;
    startTime: number;
    endTime: number;
    date:string;
    id:string;  

}