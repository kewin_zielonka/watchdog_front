﻿export class User {
    id:string
    email:string
    firstName: string;
    lastName: string;
    username: string;
    password:string
    role:any[];
    active:string;
    inTeam:boolean;
}