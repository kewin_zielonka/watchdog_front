import { Component, OnInit, Input, Output, EventEmitter, AfterContentInit  } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';

import { Response } from '@angular/http';
import { User } from '../../../_models/user';
import { UserService } from '../../../_services/index';

import { DialogModule } from 'primeng/primeng';
import { PasswordModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { Message } from 'primeng/primeng';

@Component({
    selector: 'user-change-password',
    styleUrls: ['user-change-password.component.css'],
    templateUrl: 'user-change-password.component.html'
})
export class UserChangePassword implements AfterContentInit {
    @Input('showUserChangePassword') showUserChangePassword: boolean;
    @Input('user') loggedInUser: User;
    @Output('closeEvent') closeUserChangePasswordEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    userChangePasswordForm: FormGroup;
    loggedInUserFromDB: User;
    msgs: Message[] = [];

    constructor(private fb: FormBuilder, private userService: UserService){}

    ngAfterContentInit(){
        this.userChangePasswordForm = this.fb.group({
            'oldPassword': new FormControl('', Validators.compose([Validators.required])),
            'newPassword': new FormControl('', Validators.compose([Validators.required])),
            'reapetedNewPassword': new FormControl('', Validators.compose([Validators.required]))
        });
    }

    closeUserChangePassword(){     
        this.showUserChangePassword = false;      
        this.closeUserChangePasswordEvent.emit(this.showUserChangePassword);
    }

    compareNewPasswords(){
        return this.userChangePasswordForm.controls['newPassword'].value === this.userChangePasswordForm.controls['reapetedNewPassword'].value;
    }

    onChangePasswordSubmit() {
        let rejectPasswordChange = false;
        this.loggedInUser.password = this.userChangePasswordForm.controls['oldPassword'].value;

        this.userService.checkMatchesWithEncodedPassword(this.loggedInUser).subscribe((res: Response) => {
            let rawAndEncodedPasswordsMatches: boolean = JSON.parse(res.text());  

            if(!rawAndEncodedPasswordsMatches){
                rejectPasswordChange = true;
                this.msgs.push({ severity: 'error', summary: 'Błąd', detail: 'Stare hasło nie pasuje!'});
            }    

            if(!this.compareNewPasswords()){
                rejectPasswordChange = true;
                this.msgs.push({ severity: 'error', summary: 'Błąd', detail: 'Nowe hasła różnią się!'});
            }   

            if(!rejectPasswordChange){
                this.userService.getById(this.loggedInUser.username).subscribe((res: Response) => {
                    this.loggedInUserFromDB = JSON.parse(res.text());
                    this.loggedInUserFromDB.password = this.userChangePasswordForm.controls['newPassword'].value;          
                    this.userService.changeUserPassword(this.loggedInUserFromDB).subscribe((res: Response) => {
                        this.closeUserChangePassword();  
                        this.msgs.push({ severity: 'success', summary: 'Sukces', detail: 'Pomyślnie zmieniono hasło' });             
                    }); 
                });
            }           
        })
    }
}