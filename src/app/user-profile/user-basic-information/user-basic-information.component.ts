import { Component, OnInit, Input } from '@angular/core';
import { PanelModule } from 'primeng/primeng';
import { User } from '../../_models/user';
import { InputTextModule } from 'primeng/primeng';
import { UserChangePassword } from './user-change-password/user-change-password.component';

@Component({
    selector: 'user-basic-information',
    styleUrls: ['user-basic-information.component.css'],
    templateUrl: 'user-basic-information.component.html'
})
export class UserBasicInformation {
    @Input('user') loggedInUser: User;
    disableUserDataInputs: boolean = true;
    showUserChangePassword: boolean = false;

    onChangePasswordClick(){
        this.showUserChangePassword = true;
    }

    closeUserChangePassword($event){
        this.showUserChangePassword = $event;
    }
}