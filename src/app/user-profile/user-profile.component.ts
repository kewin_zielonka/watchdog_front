import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { UserBasicInformation } from './user-basic-information/user-basic-information.component';
import { FieldsetModule } from 'primeng/primeng';
import { ToolbarModule } from 'primeng/primeng';
import { User } from '../_models/user';
import { UserService } from '../_services/user.service';

@Component({
    selector: 'user-profile',
    moduleId: module.id,
    styleUrls: ['user-profile.component.css'],
    templateUrl: 'user-profile.component.html'
})
export class UserProfile implements OnInit {
    private loggedInUser: User;

    constructor(private userService: UserService){
        this.loadLoggedInUserInformation();
    }

    ngOnInit(){
        this.userService.getById(this.loggedInUser.username).subscribe((res: Response) => {
            this.loggedInUser = JSON.parse(res.text());
        });  
    }

    private loadLoggedInUserInformation(): void {
        this.loggedInUser = JSON.parse(localStorage.getItem('currentUser'));
    }
}