import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PanelModule } from 'primeng/primeng';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

@Component({
    selector: 'user-project-team-description',
    styleUrls: ['user-project-team-description.component.css'],
    templateUrl: 'user-project-team-description.component.html'
})
export class UserProjectTeamDescription {  
    @Input('display') displayUserProjectTeam;
    @Input('users') selectedUserProjectTeam;
    @Output('hideEvent') hideUserProjectTeamEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

    hideSelectedProjectTeam(){     
        this.displayUserProjectTeam = false;        
        this.hideUserProjectTeamEvent.emit(this.displayUserProjectTeam);
    }
}