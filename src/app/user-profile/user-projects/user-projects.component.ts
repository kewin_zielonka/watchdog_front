import { Component, OnInit, Input } from '@angular/core';
import { PanelModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { Project } from '../../_models/project'
import { ProjectService } from '../../_services/project.service'
import { User } from '../../_models/user';
import { Response } from '@angular/http';
import { UserProjectTeamDescription } from './user-project-team-description/user-project-team-description.component'

@Component({
    selector: 'user-projects',
    styleUrls: ['user-projects.component.css'],
    templateUrl: 'user-projects.component.html'
})
export class UserProjects implements OnInit {  
    @Input('user') loggedInUser: User;
    projects: Project[] = [];
    displayUserProjectTeam: boolean = false;
    displayUserGenerateRaport: boolean = false;
    selectedUserProjectTeam: User[];

    constructor(private projectService: ProjectService){}

    ngOnInit(){
        this.getLoggedInUserProjects();
    }
    
    getLoggedInUserProjects(){
        this.projectService.getAll().subscribe((res: Response) => {
            this.projects =  JSON.parse(res.text());
        });     
    }

    displaySelectedProjectTeam(selectedUserProjectTeam: User[]){
        this.displayUserProjectTeam = true;
        this.selectedUserProjectTeam = selectedUserProjectTeam;
    }

    hideSelectedProjectTeam($event){
        this.displayUserProjectTeam = $event;
    }

    displayGenerateRaport(){
        this.displayUserGenerateRaport = true;
    }

    hideGenerateRaport($event){
        this.displayUserGenerateRaport = $event;
    }
}