import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { DialogModule } from 'primeng/primeng';
import { CommonModule } from '@angular/common';
import { FieldsetModule } from 'primeng/primeng';
import { DropdownModule, Dropdown } from 'primeng/primeng';
import { SelectItem } from 'primeng/primeng';
import { TabViewModule } from 'primeng/primeng';

import { Project } from '../../../_models/project'
import { RaportType } from '../../../_models/raport-generator/raport-type'
import { DownloadType } from '../../../_models/downloadType'
import { RaportGenerator } from '../../../_models/raport-generator/raport-generator'
import { RaportService } from '../../../_services/'

@Component({
    selector: 'user-projects-raport',
    styleUrls: ['user-projects-raport.component.css'],
    templateUrl: 'user-projects-raport.component.html',
})
export class UserProjectsRaport implements OnInit {
    @Input('display') displayUserGenerateRaport;
    @Input('projects') userProjects: Project[];
    @Output('hideEvent') hideUserGenerateRaportEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    private static DAY_RANGE_TO_GENERATE_RAPORT: number = 62;
    downloadType: DownloadType;
    selectedProjectId: string;
    selectedGenerateType: string;
    dropDownProjectsName: SelectItem[] = [];
    dropDownGenerateType: SelectItem[] = [];
    selectedFromDate: Date = new Date();
    selectedToDate: Date = new Date();
    disableGenerateButton: boolean;
    actuallDate: Date;
    yearPanelRangeRestriction: string;
    monthPanelRangeRestriction: string;
    minDateRangeRestriction: Date;
    maxDateRangeRestriction: Date;
    raportGenerator: RaportGenerator = new RaportGenerator();

    constructor(private raportService: RaportService){}

    ngOnInit(){
        this.initializeDropDownGenerateTypeName();
        this.initializeActuallDate();
        this.initializeMinDateRange();
        this.initializeMaxDateRange();
    }

    ngOnChanges(changes: SimpleChanges){
        if(changes['userProjects'])
            this.initializeDropDownProjectsName();
    }

    onGenerateRaportButtonClick(){
        if(this.raportGenerator.raportService == null)
            this.raportGenerator.raportService = this.raportService;

        this.raportGenerator.raportType = RaportType.PROJECT;
        this.raportGenerator.selectedId = this.selectedProjectId;       
        this.raportGenerator.fromDate = this.selectedFromDate;
        this.raportGenerator.toDate = this.selectedToDate;
        this.raportGenerator.downloadType = <DownloadType>DownloadType[this.selectedGenerateType];
        this.raportGenerator.generateDownloadLink();
        this.raportGenerator.generateRaport();
    }

    initializeActuallDate(){
        let today = Date.now();
        this.actuallDate = new Date(today);
    }

    initializeMinDateRange(){
        this.minDateRangeRestriction = new Date(this.actuallDate);
        this.minDateRangeRestriction.setDate(this.minDateRangeRestriction.getDate() - UserProjectsRaport.DAY_RANGE_TO_GENERATE_RAPORT);
    }

    initializeMaxDateRange(){
        this.maxDateRangeRestriction = new Date(this.actuallDate);
    }

    initializeDropDownProjectsName(){
        if(this.userProjects != null){
            for(let project of this.userProjects)
                this.dropDownProjectsName.push({label:project.name, value: project.id});
        }
        if(this.dropDownProjectsName[0] != undefined)
            this.selectedProjectId = this.dropDownProjectsName[0].value;
    }

    initializeDropDownGenerateTypeName(){
        for(const downloadType in DownloadType)
            this.dropDownGenerateType.push({label: downloadType, value: downloadType});  
        
        this.selectedGenerateType = this.dropDownGenerateType[0].value;
    }

    hideUserGenerateRaport(){   
        this.displayUserGenerateRaport = false;
        this.hideUserGenerateRaportEvent.emit(this.displayUserGenerateRaport);
    }
}