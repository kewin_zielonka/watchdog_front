import { Component, Input } from '@angular/core';

import { SharedModule } from 'primeng/primeng';

@Component({
    selector: 'user-role-description',
    styleUrls: ['user-role-description.component.css'],
    templateUrl: 'user-role-description.component.html',
})
export class UserRoleDescription {
    @Input('description') selectedRoleDescription;
}