import { Component, OnInit, Input, AfterContentInit } from '@angular/core';
import { Response } from '@angular/http';
import { DataListModule } from 'primeng/primeng';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { TooltipModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { OverlayPanelModule } from 'primeng/primeng';
import { FieldsetModule } from 'primeng/primeng';

import { Role } from '../../_models/role';
import { User } from '../../_models/user';
import { RoleService } from '../../_services/';
import { UserRoleDescription } from './user-role-description/user-role-description.component';

@Component({
    selector: 'user-roles',
    styleUrls: ['user-roles.component.css'],
    templateUrl: 'user-roles.component.html',
})
export class UserRoles {
    @Input('user') loggedInUser: User; 
    selectedRoleDescription: string;
}