﻿import { Component, OnInit, Renderer, ElementRef } from '@angular/core';

import { Customer, Validation, RolesEnum } from '../_models/index';
import { CustomersService, PermissionsEngine } from '../_services/index';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { InputTextModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { InputTextareaModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { Message } from 'primeng/primeng';
import {ConfirmDialogModule,ConfirmationService} from 'primeng/primeng';
import {TooltipModule} from 'primeng/primeng';

@Component({
    moduleId: module.id,
    templateUrl: 'customers.component.html',
    providers: [ConfirmationService]
})

export class CustomersComponent implements OnInit {
    model: any = {};
    inputDisable: any = {};
    msgs: Message[] = [];
    validation: Validation[] = [];
    customers: Customer[];
    customer: Customer;
    display: boolean = false;
    create: boolean = true;
    rolesEnum = RolesEnum;

    constructor(private _permissionsEngine: PermissionsEngine, private customersService: CustomersService, el: ElementRef, renderer: Renderer, private confirmationService: ConfirmationService) {
        this.loadAllCustomers();
    }

    ngOnInit() {
    }

    showDeleteCustomerConfirm(role: string){
        this.confirmationService.confirm({
            message: 'Czy napewno chcesz usunąć klienta?',
            header: 'Wymagane potwierdzenie',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.deleteCustomer(role);
            },
            reject: () => {
                this.msgs = [{severity:'warn', summary:'Brak potwierdzenia', detail:'Nie zezwolono na usunięcie'}];
            }
        });
    }

    clearCustomerUI() {
        this.display = false;
        this.model = {};
        this.create = true;
        this.inputDisable = {};
    }
    createCustomerUI(customer: Customer) {
        this.create = false;
        this.model = customer;
        this.inputDisable.name = true;
        this.showDialog();
    }
    addValidation() {
        this.validation = [];
        this.validation.push({ value: this.model.name, detail: 'Wypełnij pole nazwy' });
        this.validation.push({ value: this.model.city, detail: 'Wypełnij pole miasta' });
        this.validation.push({ value: this.model.country, detail: 'Wypełnij pole państwa' });
        this.validation.push({ value: this.model.street, detail: 'Wypełnij pole ulicy' });
        this.validation.push({ value: this.model.houseNumber, detail: 'Wypełnij pole nr domu' });
        this.validation.push({ value: this.model.zipCode, detail: 'Wypełnij pole kodu pocztowego' });
    }
    displayButtonAction() {
        this.addValidation();
        this.validation.forEach(function (valid) {
            if (valid.value == null || valid.value == "") {
                this.msgs.push({ severity: 'error', summary: 'Błąd', detail: valid.detail });
            }
        }, this);
        if (this.msgs.length == 0) {
            if (this.create) {
                this.customersService.create(this.model).subscribe((res: Response) => {
                    this.loadAllCustomers();
                });
                this.loadAllCustomers();

            }
            else {
                this.customersService.update(this.model).subscribe((res: Response) => {
                    this.loadAllCustomers();
                });
                this.loadAllCustomers();
            }
            this.clearCustomerUI();
        }
    }

    deleteCustomer(role: string) {
        var response = this.customersService.delete(role);
        response.subscribe((res: Response) => {
            if (res.text() == "false") {
                this.msgs.push({ severity: 'error', summary: 'Błąd', detail: "Nie można usunąć klienta, klient ma powiązania" });
            }
            else {
                this.msgs.push({ severity: 'success', summary: 'Udało się', detail: 'Klient został usunięty' });
            }
            this.loadAllCustomers()
        });
    }
    showDialog() {
        this.display = true;
    }
    editCustomer(customer: string) {
        this.customersService.getById(customer).subscribe((res: Response) => {
            this.customer = JSON.parse(res.text());

            this.createCustomerUI(this.customer);
        }
        );
    }
    private loadAllCustomers() {
        this.customersService.getAll().subscribe((res: Response) => {
            this.customers = JSON.parse(res.text());
        }
        );
    }
}