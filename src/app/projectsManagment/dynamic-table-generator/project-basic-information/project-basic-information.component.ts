import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { PanelModule } from 'primeng/primeng';
import { ProjectUrlParameters } from '../../../_models/project-url-parameters'

@Component({
    selector: 'project-basic-information',
    styleUrls: ['project-basic-information.component.css'],
    templateUrl: 'project-basic-information.component.html'
})
export class ProjectBasicInformation implements OnInit {
    projectUrlParameters: ProjectUrlParameters = new ProjectUrlParameters();

    constructor(private route: ActivatedRoute){}

    ngOnInit(){
        this.route.params.subscribe(param =>{
            this.projectUrlParameters.projectName = param['_projectName'];
            this.projectUrlParameters.customerName = param['_customerName'];
            this.projectUrlParameters.departmentName = param['_departmentName'];
            this.projectUrlParameters.projectManagerFirstAndLastName = param['_projectManagerFirstAndLastName'];
        });
    }
}