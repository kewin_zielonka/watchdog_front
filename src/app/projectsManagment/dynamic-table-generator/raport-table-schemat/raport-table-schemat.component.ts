import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TableGenerator } from '../../../_models/table-generator/table-generator'
import { DynamicRow } from '../../../_models/table-generator/dynamic-row'
import { TableSchemat } from '../../../_models/table-generator/table-schemat'
import { DataTableModule ,SharedModule, DataTable, Header, Column  } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';

@Component({
    selector: 'raport-table-schemat',
    styleUrls: ['raport-table-schemat.component.css'],
    templateUrl: 'raport-table-schemat.component.html',
    encapsulation: ViewEncapsulation.None
})
export class RaportTableSchemat {
    tableGenerator: TableGenerator;
    list: any[] = [];
    i:number = 0;
    index: number = 0;;

    constructor(){
        this.tableGenerator = new TableGenerator();
        let r2: DynamicRow = new DynamicRow();
        r2.rowName = 'Nagłowki: ';
        let r3: DynamicRow = new DynamicRow();
        r3.rowName = 'Typ danych: ';
        let r1: DynamicRow = new DynamicRow();
        r1.rowName = 'Nr.: ';
        this.list.push(r1);
        this.list.push(r2);
        this.list.push(r3);
    }

    onAddColumnClick(){
        if(this.i==0){
            this.list[0].valueList.push(this.index);
            this.list[1].valueList.push('PIERWSZY');
            this.list[2].valueList.push('float');
        }else{
             this.list[0].valueList.push(this.index);
            this.list[1].valueList.push('DEFAULT');
            this.list[2].valueList.push('int');
        }
       this.i++;
       this.index++;
    }

    onDeleteColumnClick(){
        // this.tableGenerator.removeFirstColumn();
    }
}