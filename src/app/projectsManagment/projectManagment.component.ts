﻿import { Component, OnInit, Renderer, ElementRef } from '@angular/core';
import { ProjectUrlParameters } from '../_models/project-url-parameters'
import { Project, User, Department, Customer, Validation } from '../_models/index';

import { ProjectManagmentService, UserService, DepartmentsService, CustomersService, PermissionsEngine } from '../_services/index';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { InputTextModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { PickListModule } from 'primeng/primeng';
import { SelectItem } from 'primeng/primeng';
import { ConvertToSelectedItems } from '../helpers/index';
import { RolesEnum } from '../_models/index';
import { TooltipModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { Message } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    moduleId: module.id,
    templateUrl: 'projectManagment.component.html',
})

export class ProjectManagmentComponent implements OnInit {
    model: any = {};
    inputDisable: any = {};

    project: Project
    projects: Project[];
    customers: SelectItem[];

    projectManagers: SelectItem[];
    departments: SelectItem[];
    users: User[] = [];
    usersInTeam: User[] = [];
    customersList: Customer[];

    departmentsList: Department[];
    msgs: Message[] = [];
    validation: Validation[] = [];
    rolesEnum = RolesEnum;

    teams: boolean;
    display: boolean = false;
    create: boolean = true;
    constructor(private router: Router, private _permissionsEngine: PermissionsEngine, private projectService: ProjectManagmentService, private userService: UserService, private customersService: CustomersService, private departmentsService: DepartmentsService, private converter: ConvertToSelectedItems, el: ElementRef, renderer: Renderer
    ) {
        this.loadAllProjects();
        this.loadItems();
        this.inputDisable.customer = true;
        this.addValidation();
    }

    ngOnInit() {    
    }

    navigateToTableGenerator(project: Project){
        let projectUrlParameters : ProjectUrlParameters = new ProjectUrlParameters();
        projectUrlParameters.copySelectedInformation(project);
        this.router.navigate(['/projectsManagment/dynamicTableGenerator', projectUrlParameters]);
    }

    clearProjectUI() {
        this.display = false;
        this.model = {};
        this.create = true;

    }
    addValidation() {
        this.validation = [];
        this.validation.push({ value: this.model.name, detail: 'Wypełnij pole nazwy' });
        this.validation.push({ value: this.model.customer, detail: 'Wypełnij pole klienta' });
        this.validation.push({ value: this.model.department, detail: 'Wypełnij pole działu' });
        this.validation.push({ value: this.model.projectManager, detail: 'Wypełnij pole PM' });

    }
    displayButtonAction() {
        this.msgs = [];
        this.addValidation();
        this.validation.forEach(function (valid) {
            if (valid.value == null || valid.value == "") {
                this.msgs.push({ severity: 'error', summary: 'Błąd', detail: valid.detail });
            }
        }, this);
        if (this.msgs.length == 0) {
            if (this.create) {
                this.projectService.create(this.model).subscribe((res: Response) => {
                    this.loadAllProjects();
                });
            }
            else {
                this.projectService.update(this.model).subscribe((res: Response) => {
                    this.loadAllProjects();
                });
            }
            this.clearProjectUI();

        }
    }

    showDialog() {
        this.display = true;
    }
    private loadAllProjects() {
        this.projectService.getAll().subscribe((res: Response) => {
            this.projects = JSON.parse(res.text());
        }

        );
    }
    createProjectUI(project: Project) {
        this.model.id = project.id;
        this.model.name = project.name;
        this.model.customer = project.customer.id;
        if (project.accountant != null) {
            this.model.accountant = project.accountant.id;
        }
        this.model.department = project.department.id;
        this.model.projectManager = project.projectManager.id;


        this.showDialog();
    }
    editProject(id: string) {
        this.projectService.getById(id).subscribe((res: Response) => {
            this.project = JSON.parse(res.text());
            this.createProjectUI(this.project);
            this.create = false;
        }
        );
    }
    deleteProject(id: string) {
        this.projectService.delete(id).subscribe(() => { this.loadAllProjects() });
    }
    loadItems() {

        this.customersService.getAll().subscribe((res: Response) => {
            this.customersList = JSON.parse(res.text());
            this.customers = this.converter.convertCustomers(this.customersList);

        }
        );
        this.userService.getAll().subscribe((res: Response) => {
            var usersFromDB = JSON.parse(res.text());
            for (const user of usersFromDB) {
                if (user.active == "1") {
                    this.users.push(user);
                }
            }
            this.projectManagers = this.converter.convertUser(usersFromDB, RolesEnum.PROJECT_MANAGER);
        }
        );
        this.departmentsService.getAll().subscribe((res: Response) => {
            this.departmentsList = JSON.parse(res.text());
            this.departments = this.converter.convertDependency(this.departmentsList);

        }
        );
    }
    changeStatusProject(id: string) {
        this.projectService.getById(id).subscribe((res: Response) => {
            this.project = JSON.parse(res.text());
            if (this.project.isActive == '0') {
                this.project.isActive = '1';
            }
            else {
                this.project.isActive = '0';

            }

            this.projectService.updateStatus(this.project).subscribe((res: Response) => {
                this.loadAllProjects();
            });
        });

    }
    changeStatusInProject(projectId: string, userId: string) {
        var userData = { projectId: projectId, userId: userId }
        this.projectService.updateStatusInProject(userData).subscribe((res: Response) => {
            this.projectService.getById(this.project.id).subscribe((res: Response) => {
                this.project = JSON.parse(res.text());
                this.addUserToTeam(this.project)
            }
            );
        });


    }
    saveTeam() {
        let teamToSave = { id: this.project.id, users: [] };
        teamToSave.users = [];
        for (const teamUser of this.usersInTeam) {
            teamToSave.users.push(teamUser.id)
        }
        this.projectService.saveTeam(teamToSave).subscribe(() => { this.loadAllProjects() });
        this.teams = false;
    }
    close() {
        this.teams = false;
        this.loadAllProjects();

    }
    private addUserToTeam(project: Project) {

        this.teams = true;
        this.userService.getAll().subscribe((res: Response) => {
            var usersFromDB = JSON.parse(res.text());
            this.users = [];
            for (const user of usersFromDB) {
                if (user.active == "1") {
                    this.users.push(user);
                }
            }
            this.project = project;
            this.usersInTeam = project.team;
            for (const teamUser of this.usersInTeam) {
                teamUser.inTeam = true;
                for (const user of this.users) {
                    if (user.id == teamUser.id) {
                        var index = this.users.indexOf(user, 0);
                        this.users.splice(index, 1);
                    }
                }
            }
        }
        );
    }
    rowStyles(rowData: any, rowIndex: number): string {
        let style: string;
        if (rowData.isActive == '0') {
            style = 'red';
        }
        else {
            style = 'green';

        }
        return style;
    }

}