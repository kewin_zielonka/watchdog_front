import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { AlertService, AuthenticationService, UserService } from '../_services/index';
import { User } from '../_models/index';

import {ButtonModule} from 'primeng/primeng';
import {InputTextModule} from 'primeng/primeng';
import {PasswordModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html'
})
 
export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    msgs: Message[] = [];
 
    constructor(
            private userService:UserService,

        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService) { }
 
    ngOnInit() {
        // reset login status
       
        this.authenticationService.logout();
 
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }
 
    login() {
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
                data => {
                    this.userService.getById(this.model.username).subscribe((res:Response) => {  
                        let user = JSON.parse(res.text());                 
                        localStorage.setItem('currentUser', res.text());
                        this.router.navigate([this.returnUrl]);
                    }
                );                
                },
                error => {                      
                    if(error.status === 401)   {
                        this.msgs.push({severity:'error', summary:'Błąd autoryzacji', detail:'Nie poprawny login lub hasło'});
                    }else if(error.status === 404) {
                        this.msgs.push({severity:'error', summary:'Błąd', detail:'Nie można wyświetlić strony'});
                    }else{
                        this.msgs.push({severity:'error', summary:'Błąd serwera', detail:'Brak połączenia z serwerem'});
                    }                          
                        
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}