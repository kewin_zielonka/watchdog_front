import { WatchdogPage } from './app.po';

describe('watchdog App', function() {
  let page: WatchdogPage;

  beforeEach(() => {
    page = new WatchdogPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
